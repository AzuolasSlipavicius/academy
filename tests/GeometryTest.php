<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\Tests;

use Aslipivicius\Academy\geometry\CircleGeometry;
use Aslipivicius\Academy\geometry\RectangleGeometry;
use Aslipivicius\Academy\geometry\RhombusGeometry;
use Aslipivicius\Academy\geometry\SquareGeometry;
use PHPUnit\Framework\TestCase;

class GeometryTest extends TestCase
{
    public function testCircle(): void
    {
        $r = 12;
        $shape = new CircleGeometry($r);
        $this->assertEquals($r * $r * M_PI, $shape->calculateArea());
        $this->assertEquals($r * 2 * M_PI, $shape->calculatePerimeter());
    }

    public function testRectangle(): void
    {
        $a = 2.5;
        $b = 3;

        $shape = new RectangleGeometry($a, $b);
        $this->assertEquals($a * $b, $shape->calculateArea());
        $this->assertEquals(($a +$b) * 2, $shape->calculatePerimeter());
    }

    public function testRhombus(): void
    {
        $a = 2.5;
        $b = 5;

        $shape = new RhombusGeometry($a, $b);
        $this->assertEquals($a * $b, $shape->calculateArea());
        $this->assertEquals($a * 4, $shape->calculatePerimeter());
    }

    public function testSquare(): void
    {
        $a = 2.5;

        $shape = new SquareGeometry($a);
        $this->assertEquals($a * $a, $shape->calculateArea());
        $this->assertEquals($a * 4, $shape->calculatePerimeter());
    }

}