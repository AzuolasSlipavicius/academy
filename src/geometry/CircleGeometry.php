<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\geometry;

class CircleGeometry implements ShapeInterface
{

    public function __construct(private float $radius)
    {
    }

    public function calculateArea(): float
    {
        return $this->radius * $this->radius * M_PI;
    }

    public function calculatePerimeter(): float
    {
        return $this->radius * 2 * M_PI;
    }
}