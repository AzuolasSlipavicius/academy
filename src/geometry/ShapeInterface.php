<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\geometry;

interface ShapeInterface
{
    public function calculateArea(): float;

    public function calculatePerimeter(): float;

}