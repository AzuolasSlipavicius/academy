<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\geometry;

class RhombusGeometry implements ShapeInterface
{
    public function __construct(private float $border, private float $high)
    {
    }

    public function calculateArea(): float
    {
        return $this->border * $this->high;
    }

    public function calculatePerimeter(): float
    {
        return $this->border * 4;
    }
}