<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\geometry;

class RectangleGeometry implements ShapeInterface
{
    public function __construct(private float $firstBorder, private float $secondBorder)
    {
    }

    public function calculateArea(): float
    {
        return $this->firstBorder * $this->secondBorder;
    }

    public function calculatePerimeter(): float
    {
        return ($this->firstBorder + $this->secondBorder) * 2;
    }
}