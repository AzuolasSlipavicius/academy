<?php
declare(strict_types=1);

namespace Aslipivicius\Academy\geometry;

class SquareGeometry implements ShapeInterface
{
    public function __construct(private float $border)
    {
    }

    public function calculateArea(): float
    {
        return $this->border * $this->border;
    }

    public function calculatePerimeter(): float
    {
        return $this->border * 4;
    }
}